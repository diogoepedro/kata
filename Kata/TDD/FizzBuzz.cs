﻿namespace Kata.TDD
{
    public class FizzBuzz
    {
        public static string SearchFizzBuzzByNumber(int number)
        {
            string toReturn = "";
            bool isNumber = true;

            if (number % 3 == 0)
            {
                toReturn += "Fizz";
                isNumber = false;
            }

            if (number % 5 == 0)
            {
                toReturn += "Buzz";
                isNumber = false;
            }

            if (isNumber)
            {
                return number.ToString();
            }

            return toReturn;
        }
    }
}
