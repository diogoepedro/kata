﻿namespace Kata.TDD
{
    public class ChristmasLights
    {
        public int[,] Lights { get; set; }

        public ChristmasLights()
        {
            Lights = new int[1000, 1000];
        }

        public void TurnOnLights(int initialX, int finalX, int initialY, int finalY)
        {
            for (int i = initialX; i < finalX + 1; i++)
            {
                for (int j = initialY; j < finalY + 1; j++) 
                {
                    Lights[i, j] += 1;
                }
            }
        }

        public void ToggleLights(int initialX, int finalX, int initialY, int finalY)
        {
            for (int i = initialX; i < finalX + 1; i++)
            {
                for (int j = initialY; j < finalY + 1; j++)
                {
                    Lights[i, j] += 2;
                }
            }
        }

        public void TurnOffLights(int initialX, int finalX, int initialY, int finalY)
        {
            for (int i = initialX; i < finalX + 1; i++)
            {
                for (int j = initialY; j < finalY + 1; j++)
                {
                    if (Lights[i, j] == 0)
                        continue;

                    Lights[i, j] -= 1;
                }
            }
        }
    }
}
