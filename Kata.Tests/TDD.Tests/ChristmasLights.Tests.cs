﻿using Kata.TDD;

namespace Kata.Tests.TDD.Tests
{
    public class ChristmasLightsTests
    {
        ChristmasLights christmasLights;

        [SetUp]
        public void SetUp()
        {
            christmasLights = new ChristmasLights();
        }

        [Test]
        public void TurnOnLights_AllLightsOn_IsTrue()
        {
            christmasLights.TurnOnLights(0, 999, 0, 999);

            var result = GetTotalBrightness() == 1_000_000;

            Assert.That(result, Is.True);
        }

        [Test]
        public void ToggleLights_AllLightsWereToggled_IsTrue()
        {
            christmasLights.ToggleLights(0, 999, 0, 999);

            var result = GetTotalBrightness() == 2_000_000;

            Assert.That(result, Is.True);
        }

        [Test]
        public void TurnOffLights_FirstColumnIsOff_IsTrue()
        {
            christmasLights.TurnOnLights(0, 999, 0, 999);
            christmasLights.TurnOffLights(0, 999, 0, 0);

            var result = GetTotalBrightness() == 999_000;

            Assert.That(result, Is.True);
        }

        private int GetTotalBrightness()
        {
            int totalBrightness = 0;

            foreach (var light in christmasLights.Lights)
            {
                totalBrightness += light;
            }

            return totalBrightness;
        }
    }
}
