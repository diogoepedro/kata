﻿using Kata.TDD;

namespace Kata.Tests.TDD.Tests
{
    public class FizzBuzzTests
    {
        [Test]
        public void FizzBuzz_IsNumber_IsTrue()
        {
            string fizzBuzzString = FizzBuzz.SearchFizzBuzzByNumber(2);

            var result = fizzBuzzString.Equals("2");

            Assert.That(result, Is.True);
        }

        [Test]
        public void FizzBuzz_IsFizz_IsTrue()
        {
            string fizzBuzzString = FizzBuzz.SearchFizzBuzzByNumber(3);

            var result = fizzBuzzString.Equals("Fizz");

            Assert.That(result, Is.True);
        }

        [Test]
        public void FizzBuzz_IsBuzz_IsTrue()
        {
            string fizzBuzzString = FizzBuzz.SearchFizzBuzzByNumber(5);

            var result = fizzBuzzString.Equals("Buzz");

            Assert.That(result, Is.True);
        }

        [Test]
        public void FizzBuzz_IsFizzBuzz_IsTrue()
        {
            string fizzBuzzString = FizzBuzz.SearchFizzBuzzByNumber(15);

            var result = fizzBuzzString.Equals("FizzBuzz");

            Assert.That(result, Is.True);
        }
    }
}
