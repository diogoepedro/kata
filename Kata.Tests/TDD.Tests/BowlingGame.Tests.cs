using Kata.TDD;

namespace Kata.Tests.TDD.Tests
{
    public class BowlingGameTests
    {
        BowlingGame bowlingGame;

        [SetUp]
        public void Setup()
        {
            bowlingGame = new BowlingGame();
        }

        [Test]
        public void TestGame()
        {
            RollMany(20, 0);

            var result = bowlingGame.Score() == 0;

            Assert.That(result, Is.True);
        }

        [Test]
        public void TestAllOnes()
        {
            RollMany(20, 1);

            var result = bowlingGame.Score() == 20;

            Assert.That(result, Is.True);
        }

        [Test]
        public void TestOneSpare()
        {
            RollSpare();
            bowlingGame.Roll(3); // other frame
            RollMany(17, 0);

            var result = bowlingGame.Score() == 16;

            Assert.That(result, Is.True);
        }

        [Test]
        public void TestOneStrike()
        {
            RollStrike();
            bowlingGame.Roll(3);
            bowlingGame.Roll(4);
            RollMany(16, 0);

            var result = bowlingGame.Score() == 24;

            Assert.That(result, Is.True);
        }

        [Test]
        public void TestPerfectGame()
        {
            RollMany(12, 10);

            var result = bowlingGame.Score() == 300;

            Assert.That(result, Is.True);
        }


        private void RollMany(int n, int pins)
        {
            for (int i = 0; i < n; i++)
            {
                bowlingGame.Roll(pins);
            }
        }

        private void RollSpare()
        {
            bowlingGame.Roll(5);
            bowlingGame.Roll(5);
        }

        private void RollStrike()
        {
            bowlingGame.Roll(10);
        }
    }
}